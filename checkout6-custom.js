import { searchDocument } from 'vtex-api';

const SELLER_CODE_CACHE = 'malweeSellerCode';
const getSellerCode = () => {
  if (vtexjs?.checkout?.orderForm?.openTextField?.value?.length) {
    const sellerCode = localStorage?.getItem(SELLER_CODE_CACHE);

    return sellerCode?.length ? sellerCode : '';
  }

  return '';
};

const sellerCode = {
  init() {
    sellerCode.main();
  },

  main() {
    $(window).on('load', () => {
      if (!document.getElementById('sellerCodeForm')) {
        const referenceNode = document.querySelector('.forms.coupon-column.summary-coupon-wrap.span7.pull-left');
        referenceNode.appendChild(sellerCode.createForm());
      }
    });
  },

  createForm() {
    const sellerCodeWrapper = document.createElement('div');
    const sellerCodeForm = document.createElement('form');
    const sellerCodeLabel = document.createElement('label');
    const sellerInput = document.createElement('input');
    const sellerCodeBtn = document.createElement('button');
    const sellerCodeRemoveBtn = document.createElement('button');
    const sellerCodeStatusMessage = document.createElement('span');

    const sellerCodeFromOrderForm = getSellerCode();

    sellerCodeWrapper.classList.add('x-seller-code__wrapper');
    sellerCodeForm.classList.add('x-seller-code__form');
    sellerCodeLabel.classList.add('x-seller-code__label');
    sellerInput.classList.add('x-seller-code__input');
    sellerCodeBtn.classList.add('x-seller-code__submit');
    sellerCodeRemoveBtn.classList.add('x-seller-code__remove');
    sellerCodeStatusMessage.classList.add('x-seller-code__message');

    sellerCodeForm.setAttribute('id', 'sellerCodeForm');
    sellerCodeForm.addEventListener('submit', this.handleSubmit);

    sellerCodeLabel.setAttribute('for', 'sellercode');
    sellerCodeLabel.textContent = 'Código do Vendedor';

    sellerInput.setAttribute('id', 'sellerInput');
    sellerInput.setAttribute('name', 'sellercode');
    sellerInput.setAttribute('placeholder', 'Código do Vendedor');
    sellerInput.addEventListener('keypress', this.handleInput);
    sellerInput.value = sellerCodeFromOrderForm;

    sellerCodeBtn.setAttribute('id', 'sellerCodeBtn');
    sellerCodeBtn.setAttribute('type', 'submit');
    sellerCodeBtn.setAttribute('title', 'Adicionar');
    sellerCodeBtn.textContent = 'Adicionar';

    sellerCodeRemoveBtn.setAttribute('id', 'sellerCodeRemoveBtn');
    sellerCodeRemoveBtn.setAttribute('type', 'Button');
    sellerCodeRemoveBtn.setAttribute('title', 'Excluir');
    sellerCodeRemoveBtn.textContent = 'Excluir';
    sellerCodeRemoveBtn.addEventListener('click', this.handleSellerCodeRemoval);

    if (sellerCodeFromOrderForm) {
      sellerCodeBtn.classList.add('is--inactive');
    } else {
      sellerCodeRemoveBtn.classList.add('is--inactive');
    }

    sellerCodeStatusMessage.setAttribute('id', 'sellerCodeMessage');

    sellerCodeForm.appendChild(sellerCodeLabel);
    sellerCodeForm.appendChild(sellerInput);
    sellerCodeForm.appendChild(sellerCodeBtn);
    sellerCodeForm.appendChild(sellerCodeRemoveBtn);
    sellerCodeForm.appendChild(sellerCodeStatusMessage);

    sellerCodeWrapper.appendChild(sellerCodeForm);
    return sellerCodeWrapper;
  },

  handleInput(event) {
    const statusMessage = document.getElementById('sellerCodeMessage');
    statusMessage.classList.remove('is--active');
    const charCode = (typeof event.which === 'number') ? event.which : event.keyCode;
    if (charCode >= 48 && charCode <= 57) {
      return true;
    }
    event.preventDefault();
    return false;
  },

  handleSubmit: async (event) => {
    event.preventDefault();
    const input = document.getElementById('sellerInput');
    const hasValidInput = input.value.length && !input.value.match(/\D/g);
    if (hasValidInput) {
      try {
        const checkCodeResponse = await sellerCode.checkCode(input.value);
        const isValidCode = !!checkCodeResponse.json.length;
        const { name = '', code = '' } = checkCodeResponse.json[0] || {};
        const validCodeAndContent = isValidCode && name && code;

        try {
          if (validCodeAndContent) {
            await Promise.all([
              sellerCode.sendCodeToAttachment(name, code),
              sellerCode.sendUTM.call(this, true),
            ]);
          }

          sellerCode.handleStorage(validCodeAndContent ? input.value : '');
          sellerCode.handleMessages(validCodeAndContent ? 'sent' : 'notInBase');
          sellerCode.handleButtonState(!validCodeAndContent);
          if (document.getElementById('sellerCodeMessage').textContent === 'Código Adicionado!') {
            document.getElementById('sellerCodeMessage').style.color = '#67c119';
          } else if (document.getElementById('sellerCodeMessage').textContent.includes('não encontrado')) {
            document.getElementById('sellerCodeMessage').style.color = '#67c119';
          }
        } catch (err) {
          sellerCode.handleStorage();
          sellerCode.handleMessages('error');
          if (document.getElementById('sellerCodeMessage').textContent.includes('erro')) {
            document.getElementById('sellerCodeMessage').style.color = 'red';
          } else if (document.getElementById('sellerCodeMessage').textContent.includes('não encontrado')) {
            document.getElementById('sellerCodeMessage').style.color = '#67c119';
          }
        }
      } catch (err) {
        sellerCode.handleStorage();
        sellerCode.handleMessages('error');
        if (document.getElementById('sellerCodeMessage').textContent.includes('erro')) {
          document.getElementById('sellerCodeMessage').style.color = 'red';
        }
      }
    } else {
      sellerCode.handleMessages('invalid');
      if (document.getElementById('sellerCodeMessage').textContent.includes('Inválido')) {
        document.getElementById('sellerCodeMessage').style.color = 'red';
      } else if (document.getElementById('sellerCodeMessage').textContent.includes('não encontrado')) {
        document.getElementById('sellerCodeMessage').style.color = '#67c119';
      }
    }

    return true;
  },

  handleMessages(currStatus) {
    const statusMessage = document.getElementById('sellerCodeMessage');
    const message = () => {
      switch (currStatus) {
        case 'loading':
          return 'Verificando código...';

        case 'invalid':
          return 'Código Inválido!';

        case 'notInBase':
          return 'Código não encontrado.';

        case 'valid':
          return 'Código Válido!';

        case 'validating':
          return 'Aguardando validação...';

        case 'sent':
          return 'Código Adicionado!';

        case 'removed':
          return 'Código Removido!';

        case 'error':
          return 'Ocorreu algum erro na sua solicitação, tente mais tarde.';

        default:
          return 'Ocorreu algum erro na sua solicitação, tente mais tarde.';
      }
    };
    statusMessage.textContent = message();
    statusMessage.classList.add('is--active');
  },

  handleButtonState(isAddButton) {
    const sellerCodeBtn = document.getElementById('sellerCodeBtn');
    const sellerCodeRemoveBtn = document.getElementById('sellerCodeRemoveBtn');

    if (isAddButton) {
      sellerCodeRemoveBtn.classList.add('is--inactive');
      sellerCodeBtn.classList.remove('is--inactive');
    } else {
      sellerCodeBtn.classList.add('is--inactive');
      sellerCodeRemoveBtn.classList.remove('is--inactive');
    }

    return true;
  },

  handleSellerCodeRemoval: async (event) => {
    event.preventDefault();
    sellerCode.handleMessages('loading');

    try {
      await Promise.all([
        sellerCode.sendCodeToAttachment(),
        sellerCode.sendUTM.call(this, false),
      ]);
      sellerCode.handleStorage();
      document.getElementById('sellerInput').value = '';
      sellerCode.handleMessages('removed');
      sellerCode.handleButtonState(true);
    } catch (err) {
      sellerCode.handleStorage();
      sellerCode.handleMessages('error');
      if (document.getElementById('sellerCodeMessage').textContent.includes('Inválido') || document.getElementById('sellerCodeMessage').textContent.includes('erro')) {
        document.getElementById('sellerCodeMessage').style.color = 'red';
      }
    }
  },

  handleStorage(value) {
    if (value) {
      localStorage.setItem(SELLER_CODE_CACHE, value);
    } else {
      localStorage.removeItem(SELLER_CODE_CACHE);
    }

    return true;
  },

  checkCode(currentSellerCode) {
    return searchDocument({
      search: {
        code: currentSellerCode,
      },
      entity: 'SC',
      fields: ['name', 'code'],
    });
  },

  sendCodeToAttachment(name = '', code = '') {
    const { getOrderForm, sendAttachment } = vtexjs?.checkout;

    try {
      return getOrderForm().done(() => sendAttachment('openTextField', {
        value: name && code ? `Vendedor - nome: ${name} / código: ${code}` : '',
      }));
    } catch (err) {
      throw new Error('Failed to set openTextField', err);
    }
  },

  sendUTM(mustAdd) {
    const { orderForm, sendAttachment } = vtexjs?.checkout;
    const { marketingData } = orderForm;
    const newMarketingData = {
      ...marketingData,
      utmSource: mustAdd ? 'vendedor' : 'no utmSource',
      utmCampaign: mustAdd ? 'desconto-vendedor' : 'no utmCampaign',
    };

    sellerCode.handleMessages('validating');

    try {
      return sendAttachment('marketingData', newMarketingData);
    } catch (err) {
      throw new Error('Failed to set marketingData', err);
    }
  },
};

export default sellerCode.init;
